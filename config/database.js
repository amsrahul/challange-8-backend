const config = new URL(process.env.DATABASE_URL || "postgres://postgres:postgres@127.0.0.1:5432/challenge_8")
// console.log("protocol", config.protocol)
// console.log("hostname", config.hostname)
// console.log("username", config.username)
// console.log("password", config.password)
// console.log("port", config.port)
// console.log("path", config.pathname.replace("/", ""))



const {
  DB_USER = config.username.replace("/", ""),
  DB_PASSWORD = config.username,
  DB_NAME = config.username,
  DB_HOST = config.hostname,
  DB_PORT = config.port,
} = process.env;

module.exports = {
  development: {
    username: DB_USER,
    password: DB_PASSWORD,
    database: DB_NAME,
    host: DB_HOST,
    port: DB_PORT,
    dialect: "postgres"
  },
  test: {
    username: DB_USER,
    password: DB_PASSWORD,
    database: `${DB_NAME}_test`,
    host: DB_HOST,
    port: DB_PORT,
    storage: ":memory:",
    dialect: "sqlite"
  },
  production: {
    username: DB_USER,
    password: DB_PASSWORD,
    database: DB_NAME,
    host: DB_HOST,
    port: DB_PORT,
    dialect: "postgres"
  }
}
